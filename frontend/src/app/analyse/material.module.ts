import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule, MatCardModule, MatDividerModule, MatExpansionModule, MatGridListModule, MatIconModule, MatListModule, MatTableModule, MatToolbarModule} from '@angular/material';

@NgModule({
  imports: [CommonModule, MatButtonModule, MatListModule, MatToolbarModule, MatCardModule, MatDividerModule, MatGridListModule, MatTableModule, MatIconModule, MatExpansionModule],
  exports: [CommonModule, MatButtonModule, MatListModule, MatToolbarModule, MatCardModule, MatDividerModule, MatGridListModule, MatTableModule, MatIconModule, MatExpansionModule],
})
export class CustomMaterialModule {
}
