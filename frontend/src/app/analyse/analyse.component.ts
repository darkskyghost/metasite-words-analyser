import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FileUploader} from "ng2-file-upload";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-file-upload',
  templateUrl: './analyse.component.html',
  styleUrls: ['./analyse.component.css']
})
export class AnalyseComponent implements OnInit {

  uploadForm: FormGroup;

  results: Map<string, Map<string, number>>;

  public uploader: FileUploader = new FileUploader({
    isHTML5: true
  });
  title: string = 'File Upload';

  constructor(private fb: FormBuilder, private http: HttpClient) {
  }

  analyseSubmit() {
    for (let i = 0; i < this.uploader.queue.length; i++) {
      let fileItem = this.uploader.queue[i]._file;
      if (fileItem.size > 10000000) {
        alert("Max single file size is 10 MB");
        return;
      }
    }
    let data = new FormData();
    for (let i = 0; i < this.uploader.queue.length; i++) {
      data.append('file', this.uploader.queue[i]._file);
    }
    this.analyse(data).subscribe(response => {
      this.uploader.clearQueue();

      let resultsMap: Map<string, number>[] = Array.from(response);
      this.results = new Map<string, Map<string, number>>();
      this.results.set("A-G", resultsMap[0]);
      this.results.set("H-N", resultsMap[1]);
      this.results.set("O-Y", resultsMap[2]);
      this.results.set("V-Z", resultsMap[3]);
    }, error => console.log(error));
  }

  analyse(data: FormData): Observable<any> {
    return this.http.post('http://localhost:8080/analyse', data);
  }

  ngOnInit() {
    this.uploadForm = this.fb.group({
      document: [null, [Validators.required]]
    });
  }
}
