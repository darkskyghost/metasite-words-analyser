# Words Analyser Backend
## Description
Analyses .txt files, reads words and their counts

Then outputs words to .txt with their counts in separate categories based on first letter

## App arguments
By default files are read and written to current directory

In order to override set: 

Input directory path:
`input.directory.path`

Input file path: 
`input.file.path`

Output directory path:
`output.directory.path`

