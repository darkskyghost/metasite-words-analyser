package lt.juodamorka.metasite.pojo;

public enum WordsCategoryEnum {

    AG("a-g", "(?i)^[a-g].*"),
    HN("h-n", "(?i)^[h-n].*"),
    OU("o-u", "(?i)^[o-u].*"),
    VZ("v-z", "(?i)^[v-z].*");

    private String name;
    private String regex;

    WordsCategoryEnum(String name, String regex) {
        this.name = name;
        this.regex = regex;
    }

    public String getName() {
        return name;
    }

    public String getRegex() {
        return regex;
    }
}
