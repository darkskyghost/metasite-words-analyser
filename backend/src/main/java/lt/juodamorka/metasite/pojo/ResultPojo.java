package lt.juodamorka.metasite.pojo;

import java.util.HashMap;
import java.util.Map;

public class ResultPojo {

    private Map<WordsCategoryEnum, Map<String, Long>> wordsMap;

    public ResultPojo() {
        this.wordsMap = new HashMap<>();
    }

    public Map<WordsCategoryEnum, Map<String, Long>> getWordsMap() {
        return wordsMap;
    }

    public void setWordsMap(Map<WordsCategoryEnum, Map<String, Long>> wordsMap) {
        this.wordsMap = wordsMap;
    }
}
