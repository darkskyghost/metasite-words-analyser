package lt.juodamorka.metasite.service.analysis;

import lt.juodamorka.metasite.pojo.ResultPojo;
import lt.juodamorka.metasite.pojo.WordsCategoryEnum;
import lt.juodamorka.metasite.service.file.FileReaderService;
import lt.juodamorka.metasite.service.file.FileWriterService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AnalyserService implements InitializingBean {

    private static final String wordRegex = "\\W+";

    private Logger logger = Logger.getLogger(getClass().getName());

    private ExecutorService executorService = Executors.newFixedThreadPool(4, new CustomizableThreadFactory("words-processor-task-"));

    @Autowired
    private FileReaderService fileReaderService;

    @Autowired
    private FileWriterService fileWriterService;

    @Value("${input.directory.path}")
    private String directoryPath;

    @Value("${input.file.path}")
    private String filePath;

    /**
     * Runs during app startup
     * Checks for files in directory or for single file
     * Otherwise does nothing
     *
     * @throws Exception any
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, Long> wordsMap = new ConcurrentHashMap<>();
        List<ProcessingTask> tasks = new ArrayList<>();

        if (!directoryPath.isEmpty()) {
            logger.info("Analysing files in directory: " + directoryPath);

            try (Stream<Path> walk = Files.walk(Paths.get(directoryPath))) {
                List<String> filePaths = walk.map(Path::toString).filter(f -> f.endsWith(".txt")).collect(Collectors.toList());
                for (String singleFilePath : filePaths) {
                    tasks.add(new ProcessingTask(singleFilePath, null));
                }
            }
        }

        if (!filePath.isEmpty()) {
            logger.info("Analysing single file: " + filePath);
            if (!filePath.endsWith(".txt")) {
                logger.log(Level.INFO, filePath + " is not a text file, skipping");
            }
            tasks.add(new ProcessingTask(filePath, null));
        }

        List<Future<Map<String, Long>>> futures = executorService.invokeAll(tasks);

        for (Future<Map<String, Long>> future : futures) {
            wordsMap.putAll(future.get());
        }

        if (!wordsMap.isEmpty()) {
            fileWriterService.writeResultToFiles(getResultPojo(wordsMap));
            logger.info("Processing finished");
        } else {
            logger.info("Nothing to process");
        }
    }

    /**
     * Analyses files
     *
     * @param files to analyse
     * @return list of analysed words maps
     */
    public List<Map<String, Long>> analyseFiles(List<MultipartFile> files) {
        Map<String, Long> wordsMap = new ConcurrentHashMap<>();
        List<ProcessingTask> tasks = new ArrayList<>();
        files.forEach(file -> {
            try {
                tasks.add(new ProcessingTask(null, file.getInputStream()));
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Cannot read file input stream", e);
            }
        });

        try {
            List<Future<Map<String, Long>>> futures = executorService.invokeAll(tasks);
            for (Future<Map<String, Long>> future : futures) {
                wordsMap.putAll(future.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            logger.log(Level.SEVERE, "Interrupted while waiting", e);
        }

        ResultPojo resultPojo = getResultPojo(wordsMap);
        fileWriterService.writeResultToFiles(resultPojo);

        List<Map<String, Long>> resultList = new LinkedList<>();
        resultList.add(resultPojo.getWordsMap().get(WordsCategoryEnum.AG));
        resultList.add(resultPojo.getWordsMap().get(WordsCategoryEnum.HN));
        resultList.add(resultPojo.getWordsMap().get(WordsCategoryEnum.OU));
        resultList.add(resultPojo.getWordsMap().get(WordsCategoryEnum.VZ));

        return resultList;
    }

    private Map<String, Long> getWordsMap(List<String> fileContentsList) {
        Map<String, Long> wordsMap = new HashMap<>();
        fileContentsList
                .stream()
                .map(fileContents -> Arrays
                        .stream(fileContents.split(wordRegex))
                        .collect(Collectors.groupingByConcurrent(Function.identity(), Collectors.counting())))
                .forEach(wordsMap::putAll);
        return wordsMap;
    }

    private ResultPojo getResultPojo(Map<String, Long> wordsMap) {
        Map<String, Long> ag = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        Map<String, Long> hn = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        Map<String, Long> ou = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        Map<String, Long> vz = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        ResultPojo resultPojo = new ResultPojo();

        wordsMap.keySet().forEach(key -> {
            if (key.matches(WordsCategoryEnum.AG.getRegex())) {
                ag.put(key, wordsMap.get(key));
            } else if (key.matches(WordsCategoryEnum.HN.getRegex())) {
                hn.put(key, wordsMap.get(key));
            } else if (key.matches(WordsCategoryEnum.OU.getRegex())) {
                ou.put(key, wordsMap.get(key));
            } else if (key.matches(WordsCategoryEnum.VZ.getRegex())) {
                vz.put(key, wordsMap.get(key));
            }
        });

        resultPojo.getWordsMap().put(WordsCategoryEnum.AG, ag);
        resultPojo.getWordsMap().put(WordsCategoryEnum.HN, hn);
        resultPojo.getWordsMap().put(WordsCategoryEnum.OU, ou);
        resultPojo.getWordsMap().put(WordsCategoryEnum.VZ, vz);

        return resultPojo;
    }

    private class ProcessingTask implements Callable<Map<String, Long>> {

        private String singleFilePath;
        private InputStream inputStream;

        ProcessingTask(String singleFilePath, InputStream inputStream) {
            this.singleFilePath = singleFilePath;
            this.inputStream = inputStream;
        }

        @Override
        public Map<String, Long> call() {
            Map<String, Long> wordsMap = new HashMap<>();
            if (singleFilePath != null) {
                wordsMap.putAll(getWordsMap(Collections.singletonList(fileReaderService.getContents(singleFilePath))));
            }
            if (inputStream != null) {
                wordsMap.putAll(getWordsMap(Collections.singletonList(fileReaderService.getContents(inputStream))));
            }

            return wordsMap;
        }
    }
}
