package lt.juodamorka.metasite.service.file;

import lt.juodamorka.metasite.pojo.ResultPojo;
import lt.juodamorka.metasite.pojo.WordsCategoryEnum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class FileWriterService {

    private Logger logger = Logger.getLogger(getClass().getName());

    private ExecutorService executorService = Executors.newFixedThreadPool(4, new CustomizableThreadFactory("file-writer-task-"));

    @Value("${output.directory.path}")
    private String outputDirectoryPath;

    /**
     * Writes results to files from ResultPojo
     * Creates a new thread for each word category
     *
     * @param pojo ResultPojo
     */
    public void writeResultToFiles(ResultPojo pojo) {
        List<FileWriterTask> tasks = new ArrayList<>();

        tasks.add(new FileWriterTask(pojo, WordsCategoryEnum.AG));
        tasks.add(new FileWriterTask(pojo, WordsCategoryEnum.HN));
        tasks.add(new FileWriterTask(pojo, WordsCategoryEnum.OU));
        tasks.add(new FileWriterTask(pojo, WordsCategoryEnum.VZ));

        try {
            executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            logger.log(Level.SEVERE, "Exception while waiting for writing completion", e);
        }
    }

    private class FileWriterTask implements Callable<Void> {

        private ResultPojo resultPojo;
        private WordsCategoryEnum category;

        FileWriterTask(ResultPojo resultPojo, WordsCategoryEnum category) {
            this.resultPojo = resultPojo;
            this.category = category;
        }

        @Override
        public Void call() {
            try {
                String outputString = resultPojo.getWordsMap().get(category)
                        .keySet()
                        .stream()
                        .map(key -> key + " " + resultPojo.getWordsMap().get(category).get(key))
                        .collect(Collectors.joining("\n"));
                Files.write(Paths.get(outputDirectoryPath + category.getName()+ "_result.txt"), outputString.getBytes());
            } catch (FileNotFoundException e) {
                logger.log(Level.SEVERE, "File not found", e);
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Error initializing stream", e);
            }

            return null;
        }
    }
}
