package lt.juodamorka.metasite.service.file;

import org.springframework.stereotype.Service;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class FileReaderService {

    private Logger logger = Logger.getLogger(getClass().getName());

    /**
     * Gets file contents from single file
     * @param path path to file
     * @return file contents
     */
    public String getContents(String path) {
        try {
            return getContents(new BufferedReader(new FileReader(new File(path))));
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE,"File not found", e);
        }

        return null;
    }

    /**
     * Gets file contents from input stream
     * @param inputStream input stream
     * @return file contents
     */
    public String getContents(InputStream inputStream) {
        return getContents(new BufferedReader(new InputStreamReader(inputStream)));
    }

    /**
     * Gets file contents from buffered reader
     * @param bufferedReader buffered reader
     * @return file contents
     */
    private String getContents(BufferedReader bufferedReader) {
        StringBuilder lines = new StringBuilder();
        String line;
        while (true) {
            try {
                if ((line = bufferedReader.readLine()) == null) break;
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Cannot read line", e);
                return null;
            }
            lines.append(line);
        }

        try {
            bufferedReader.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot close file", e);
        }

        return lines.toString();
    }
}
