package lt.juodamorka.metasite.rest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lt.juodamorka.metasite.service.analysis.AnalyserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class UploadController {

    @Autowired
    private AnalyserService analyserService;

    @PostMapping("/analyse")
    @CrossOrigin(origins = "http://localhost:4200")
    public String analyse(@RequestParam("file") List<MultipartFile> files) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(analyserService.analyseFiles(files));
    }
}
