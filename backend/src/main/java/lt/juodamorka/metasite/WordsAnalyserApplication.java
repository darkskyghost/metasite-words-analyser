package lt.juodamorka.metasite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WordsAnalyserApplication {

    public static void main(String[] args) {
        SpringApplication.run(WordsAnalyserApplication.class, args);
    }

}
