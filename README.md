# Words Analyser

### Build backend

`cd backend`

`mvn clean package`

### Build frontend

`cd frontend`

````ng build --prod````

### Run docker image

`cd docker`

`docker-compose up`

### Access

`http://localhost:4200`
